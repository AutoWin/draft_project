{
    'name': 'Son Ha Configuration',
    'version': '1.0.0',
    'category': 'Son Ha Configuration',
    'sequence': 1,
    'summary': 'Son Ha Configuration',
    'author': 'HCS Vietnam',
    'description': """
        Only use for Son Ha project
        ===========================
    """,
    'depends': [
        'purchase',
        'base'
    ],
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/purchase.xml',
        # 'views/group_product_category.xml',
    ],
    'qweb': [
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'website': '',
}
# -*- coding: utf-8 -*-
