from odoo import fields, models, api

class GroupProductCategory(models.Model):
    _name = 'sh.group.product.category'

    name = fields.Char(string="Name")
    category_ids = fields.One2many('product.category', 'sh_group_categ', string='Categories')
    # category_ids = fields.Many2many('product.category', string='Categories')