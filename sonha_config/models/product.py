from odoo import fields, models, api


class ProductCategory(models.Model):
    _inherit = 'product.category'

    sh_group_categ = fields.Many2one('sh.group.product.category', string="Categories")
