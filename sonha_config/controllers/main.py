from odoo import http
from odoo.http import request
import json


class SonHaConfiguration(http.Controller):

    @http.route('/sh_config/get_partner', type='http', auth='public')
    def get_all_partner(self):
        partners = request.env['res.partner'].search([])
        datas = []
        for partner in partners:
            datas.append({
                'id': partner.id,
                'name': partner.name
            })
        return json.dumps(datas)
